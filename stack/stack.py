from stack.exceptions import ElementDoesNotExist


class Stack(object):
    def __init__(self):
        self.stack = []

    def __len__(self):
        return len(self.stack)

    def push(self, s):
        return self.stack.append(s)

    def pop(self):
        if len(self.stack) == 0:
            raise ElementDoesNotExist
        else:
            return self.stack.pop()

    def show_list(self):
        print(self.stack)

