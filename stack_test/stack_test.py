import unittest

from stack.exceptions import ElementDoesNotExist
from stack.stack import Stack


class StackTest(unittest.TestCase):

    def test_stackShouldRaiseExceptionWhenTryingToPopNonExistingElement(self):
        s = Stack()
        self.assertRaises(ElementDoesNotExist, s.pop)

    def test_stackLenIsEqual0WhenIsEmpty(self):
        s = Stack()
        self.assertEqual(0, len(s))

    def test_stackShouldReturnAddedElementsInReverseOrder(self):
        s = Stack()
        s.push('TEST1')
        s.push('TEST2')
        s.push('TEST3')
        self.assertEqual('TEST3', s.pop())
        self.assertEqual('TEST2', s.pop())
        self.assertEqual('TEST1', s.pop())

    def test_stackLenIsEqualToNumberOfAddedElements(self):
        s = Stack()
        s.push(0)
        s.push(1)
        s.push(2)
        self.assertEqual(3, len(s))

if __name__ == '__main__':
    unittest.main()
