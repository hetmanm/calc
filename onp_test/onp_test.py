import unittest

from onp.exceptions import OnpException, OnpMissingNumericArgument, OnpMissingOperationArgument
from onp.onp import calculate_onp


class OnpTest(unittest.TestCase):

    def test_shouldReturn5WhenAdding2And3(self):
        self.assertEqual(calculate_onp('2 3 +'), 5)
        self.assertEqual(calculate_onp('3 2 +'), 5)

    def test_shouldReturn1When3Minus2(self):
        self.assertEqual(calculate_onp('3 2 -'), 1)

    def test_shouldReturnNegative1When2Minus3(self):
        self.assertEqual(calculate_onp('2 3 -'), -1)

    def test_shouldReturn6When2Times3(self):
        self.assertEqual(calculate_onp('2 3 *'), 6)
        self.assertEqual(calculate_onp('3 2 *'), 6)

    def test_shouldReturn3When6Divide2(self):
        self.assertEqual(calculate_onp('6 2 /'), 3)

    def test_shouldReturn2AndHalfWhen5Divide2(self):
        self.assertEqual(calculate_onp('5 2 /'), 2.5)

    def test_shouldRaiseExceptionWhenOnpHasMissingOperationArgument(self):
        self.assertRaises(OnpMissingOperationArgument, calculate_onp, '4')
        self.assertRaises(OnpMissingOperationArgument, calculate_onp, '4 4')

    def test_shouldRaiseExceptionWhenOnpHasMissingNumericArgument(self):
        self.assertRaises(OnpMissingNumericArgument, calculate_onp, '+')
        self.assertRaises(OnpMissingNumericArgument, calculate_onp, '+ 4')
        self.assertRaises(OnpMissingNumericArgument, calculate_onp, '4 +')

    def test_shouldReturnCorrectAnswerOnComplexCase(self):
        '''
        12 2 3 4 * 10 5 / + * + = 40
        :return:
        '''
        self.assertEqual(calculate_onp('12 2 3 4 * 10 5 / + * +'), 40)