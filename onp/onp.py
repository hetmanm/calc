def calculate_onp(onp2):

    from stack.stack import Stack
    import stack
    import onp

    # algorytm nie działa poprawnie. Nawet najprostrze działania nie dają poprawnych wyników (po to są napisane testy).
    # brakuje obsługi błędów. Po to są wyjątki, których masz używać.


    OPERATORS = ['+', '-', '*', '/']
    s = Stack()

    lista = onp2.split(" ")

    # co robi ta zmienna?
    index = None
    i = 0

    # dlaczego taka petla
    while i != len(lista):
        # po co?
        index = i
        if lista[index] == OPERATORS[0]:
            # po co te zmienne?
            num1 = s.pop()
            num2 = s.pop()
            wyn = int(num1) + int(num2)
            s.push(wyn)

        elif lista[index] == OPERATORS[1]:
            num2 = s.pop()
            num1 = s.pop()
            wyn = int(num1) - int(num2)
            s.push(wyn)

        elif lista[index] == OPERATORS[2]:
            num1 = s.pop()
            num2 = s.pop()
            wyn = int(num1) * int(num2)
            s.push(wyn)

        elif lista[index] == OPERATORS[3]:
            num2 = s.pop()
            num1 = s.pop()
            wyn = int(num1) / int(num2)
            s.push(wyn)


        else:
            s.push(lista[index])

        if stack.exceptions.ElementDoesNotExist == True:
            try:
                raise onp.exceptions.OnpMissingNumericArgument

            except:
                pass


        i += 1

    wyn = float(s.pop())

    return wyn

print(calculate_onp('2 +'))
