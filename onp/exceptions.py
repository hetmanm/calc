class OnpException(Exception):
    pass


class OnpMissingOperationArgument(OnpException):
    pass


class OnpMissingNumericArgument(OnpException):
    pass
